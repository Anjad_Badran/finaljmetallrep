package Core;

import java.io.*;
import java.lang.reflect.Array;
import java.util.*;

import jmetal.core.Solution;

public class ServicesReader {
	//public static Integer NoOfClass = 4;
	public static ArrayList<Service> ServicePool= new ArrayList<Service>();
	public static ArrayList<ServiceClass> ClassPool = new ArrayList<ServiceClass>();

	/*
	 * String ServiceId1=null; String ServiceClass1=null; String
	 * ServiceFunctionality1=null; String ServiceProvide1=null; Double
	 * ServiceCost1=(double) 0; Double SPRate1=(double) 0; int
	 * packetfiltiring1=false; int redendency1=false; Double
	 * failoverHistory1=(double) 0; int backup1=false; int
	 * encryptionStorage1=false; int encryptionTransaction1=false; int
	 * tested1=false; int LogingEnabled1=false;
	 * 
	 * Double ConfidentialitySeverity1= (double) 0; Double IntegritySeverity1
	 * =(double) 0; Double AvailabilitySeverity1 =(double) 0;
	 */
	public ServicesReader() {
		ReadServiceClass();
		ReadServices();
	
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		/* reading */

		ReadServiceClass();
		ReadServices();
	}

	/*
	 * this function Read the Services classes (Tasks) and the user preferences
	 * for CIA
	 */
	
	// Ctemp is object of type ServiceClass, and ClassPool is array list that store ServiceClass objects..

	public static  void ReadServiceClass() {
		ClassPool =new ArrayList<ServiceClass>();
		String f = "inputs/serviceClass2.txt";

		File file = new File(f);
		ServiceClass Ctemp = new ServiceClass();
		String line;
		StringTokenizer st;
	
		String token;
		try {
			FileInputStream fis = new FileInputStream(file);
			InputStreamReader isr = new InputStreamReader(fis);
			BufferedReader content = new BufferedReader(isr);
			int j = 0;
			while (content.ready()) {
				line = content.readLine();
				st = new StringTokenizer(line);
				token = st.nextToken();
				
				Ctemp = new ServiceClass();
				Ctemp.setClassid(token);
				
				ClassPool.clone();
				ClassPool.add(Ctemp);
				
				j++;
				ServicePool.clone();
			}

		} catch (Exception e) {
			System.out.println("Error: "+ e.toString());

		}
		
	}

	
	
	public static void ReadServices() {
		ServicePool = new ArrayList<Service>();
		
		String f = "inputs/servicesInfo.txt";
		
		File file = new File(f);
		Service Stemp = new Service();
		String line;
		StringTokenizer st;
		String token;
		
		try {
			FileInputStream fis = new FileInputStream(file);
			InputStreamReader isr = new InputStreamReader(fis);
			BufferedReader content = new BufferedReader(isr);
			int j = 0;
			while (content.ready()) {
				line = content.readLine();
				st = new StringTokenizer(line);
				token = st.nextToken();
				Stemp = new Service();
			
				Stemp.setServiceId(token);
				token = st.nextToken();
				Stemp.setServiceClass(token);;
				token = st.nextToken();
				Stemp.setServiceThroughput(Double.parseDouble(token));
				token = st.nextToken();
				Stemp.setServiceResponseTime(Double.parseDouble(token));
				token = st.nextToken();
				Stemp.setServiceReliability(Double.parseDouble(token));
				
				
				
	  ServicePool.add(Stemp);;
				
				
				j++;
				ServicePool.clone();
				
			}

		} 
		catch (Exception e) {
			System.out.println("error2 "+ e.toString());
			   e.printStackTrace();

		}

	
}

	}