package Core;

import jmetal.core.Variable;
import jmetal.util.PseudoRandom;

public class Service extends Variable {
	private double value_;
	private double lowerBound_;
	private double upperBound_;
	String ServiceId;
	String ServiceClass;
	double ServiceFunctionality;
	Double ServiceCost;
	Double ServiceResponseTime;
	Double ServiceReliability ;
	Double ServiceThroughput ;
	
	public Double getServiceThroughput() {
		return ServiceThroughput;
	}



	public void setServiceThroughput(Double serviceThroughput) {
		ServiceThroughput = serviceThroughput;
	}



	public Service() {
		// TODO Auto-generated constructor stub
	}
	
	
	
	public Service(double d, double e) {
		// TODO Auto-generated constructor stub
		lowerBound_ = d;
		upperBound_ = e;
		value_ = PseudoRandom.randDouble(d, e) ;
	
	}



	public String getServiceId() {
		return ServiceId;
	}
	public void setServiceId(String serviceId) {
		ServiceId = serviceId;
	}
	public String getServiceClass() {
		return ServiceClass;
	}
	public void setServiceClass(String serviceClass) {
		ServiceClass = serviceClass;
	}
	public Double getServiceCost() {
		return ServiceCost;
	}
	public void setServiceCost(Double serviceCost) {
		ServiceCost = serviceCost;
	}
	public Double getServiceFunctionality() {
		return ServiceFunctionality;
	}
	public void setServiceFunctionality(Double d) {
		ServiceFunctionality = d;
	}
	

	public Double getServiceReliability() {
		return ServiceReliability;
	}
	public void setServiceReliability(Double serviceReliability) {
		ServiceReliability = serviceReliability;
	}

	
	
	public void setServiceResponseTime(Double serviceResponseTime) {
		ServiceResponseTime = serviceResponseTime;
	}

	
	@Override
	public Variable deepCopy() {
		// TODO Auto-generated method stub
		return null;
	}



	public Double getServiceResponseTime() {
		// TODO Auto-generated method stub
		return ServiceResponseTime;
	}



	

	



	

}
