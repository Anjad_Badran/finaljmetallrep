package edu.birzeit.jmse.sbse;



import jmetal.core.Problem;
import jmetal.core.Solution;
import jmetal.encodings.solutionType.BinaryRealSolutionType;
import jmetal.encodings.solutionType.RealSolutionType;
import jmetal.metaheuristics.nsgaII.NSGAII;
import jmetal.encodings.solutionType.QoS_SOLUTION_TYPE;

import jmetal.util.JMException;
import java.util.ArrayList;

import Core.Service;


public class QoSOptimizationComposition extends Problem{
	
	/**
	 * 
	 */
	//private static final long serialVersionUID = 1L;
	
	public double globalResponseTime = 0;
	public double globalReliability = 0;
	public double globalThroughput = 0;
	public double p1 = Math.random();
	public double p2 = Math.random();
	public double p3=Math.random();
	public double p4=Math.random();
	public double p5 = Math.random();
	public double p6=Math.random();
	public double p7=Math.random();
	public double p23=Math.random();
	public int	numberOfService_;
	
	
	public static ArrayList<Service> TestedSC;
	public static ArrayList<Service> TestedSC2;
	
	
	
	public QoSOptimizationComposition(String solutionType) {
		System.out.println("I'm in QoSOptimizationComposition.java line 42");
		numberOfService_=5;
		//numberOfVariables_ = 2;
		//Value of numberOfObjectives_  result reflected on NSGAII_main
		numberOfObjectives_ = 3;
		
		numberOfConstraints_ = 0;
		//NSGAII.problemName_ = "QoSOptimizationComposition";
		
		
		//solutionType_ = new QoS_SOLUTION_TYPE(this);
		if (solutionType.compareTo("BinaryReal") == 0)
			solutionType_ = new BinaryRealSolutionType(this);
		else if (solutionType.compareTo("Real") == 0)
			solutionType_ = new RealSolutionType(this);
		
		else if (solutionType.compareTo("QoS_SOLUTION_TYPE") == 0)
		{
			System.out.println("numberOfService_: "+numberOfService_);
		
			numberOfVariables_ = 1;
			length_ = new int[numberOfVariables_];
			length_[0] = this.numberOfService_;
			System.out.println("thisssSSSSSS");
			
			solutionType_ = new QoS_SOLUTION_TYPE(this,numberOfService_);
		}
			
		
		// End Added 09-03-2015
		
		else {
			System.out.println("Error: solution type " + solutionType + " invalid");
			System.exit(-1);
		}
		
	}
	
	
	public void evaluate(Solution solution) throws JMException {
		System.out.println("evaluate(Solution solution)");
		ArrayList<Service> TestedSC=new ArrayList<Service>();
		//ArrayList<Service> TestedSC2=new ArrayList<Service>();
	//	System.out.println("I'm in evaluate meyhod in QoSOptimizationComposition.java class ");
	//	System.out.println("solution.getCompositionService().size()=  ");
	//	System.out.println("QoSOptimization.java AAAAAAA"+solution.getCompositionService().size());
		
		
		for (int i=0; i<solution.getCompositionService().size();i++)
		{  
		//	System.out.println("2222222233333333333334444444466666666");
			System.out.println("anjad take care     i=  "+solution.getCompositionService().size() );
		}
		
		for (int i=0; i<solution.getCompositionService().size();i++)
		{ 
		// add all selected concreteServices to the solution
				TestedSC.add(solution.getCompositionService().get(i));
			//	System.out.println("TeeeestedSC: "+ TestedSC.get(i).getServiceCost());

		}
		
	//	System.out.println("Teeeestedsize: "+ TestedSC.size());

		//globalCost = TestedSC.get(0).getServiceCost() + (p1*TestedSC.get(1).getServiceCost() + p2*TestedSC.get(2).getServiceCost() + p3*TestedSC.get(3).getServiceCost()  );
	    Double min1= Math.min(TestedSC.get(1).getServiceThroughput() , p2*TestedSC.get(4).getServiceThroughput());
	    Double min2= Math.min(min1,TestedSC.get(2).getServiceThroughput());
	    Double min3= Math.min(min2,TestedSC.get(3).getServiceThroughput()); 
		globalThroughput= Math.min(min3,TestedSC.get(0).getServiceThroughput());
		
		Double max= Math.max (TestedSC.get(1).getServiceResponseTime()+TestedSC.get(4).getServiceResponseTime(), TestedSC.get(2).getServiceResponseTime());
	    globalResponseTime= TestedSC.get(0).getServiceResponseTime() + Math.max (TestedSC.get(3).getServiceResponseTime(),max);
	    //globalResponseTime= TestedSC.get(0).getServiceCost()+ Math.max (TestedSC.get(1).getServiceCost()+TestedSC.get(4).getServiceCost()), TestedSC.get(2).getServiceCost() );		
		
	    globalReliability= TestedSC.get(0).getServiceReliability()* (TestedSC.get(1).getServiceReliability()*TestedSC.get(4).getServiceReliability())* TestedSC.get(2).getServiceReliability()*TestedSC.get(3).getServiceReliability();
		
		//System.out.println("Costt: "+ globalThroughput);
	
	/*
		Double min1= Math.min(TestedSC.get(0).getServiceThroughput() , p2*TestedSC.get(1).getServiceThroughput());
		Double min2= Math.min(TestedSC.get(3).getServiceThroughput(),TestedSC.get(2).getServiceThroughput());
		double th1= p2*TestedSC.get(2).getServiceThroughput()+ p4*TestedSC.get(4).getServiceThroughput();
		Double min3= Math.min(min1, th1);
		Double min4= Math.min(TestedSC.get(5).getServiceThroughput() , p2*TestedSC.get(6).getServiceThroughput());
		Double min5= Math.min(TestedSC.get(7).getServiceThroughput() ,min4);
		Double min6= Math.min(min3 ,min4);
		globalThroughput=Math.min(TestedSC.get(8).getServiceThroughput() ,min6);
		
		
		double res1= TestedSC.get(0).getServiceResponseTime()+TestedSC.get(1).getServiceResponseTime()+
				p23*(TestedSC.get(2).getServiceResponseTime()+TestedSC.get(3).getServiceResponseTime())+
				p4*TestedSC.get(4).getServiceResponseTime();
		
		Double max1= Math.max (TestedSC.get(5).getServiceResponseTime(),TestedSC.get(6).getServiceResponseTime());
		Double max2= Math.max (max1, TestedSC.get(7).getServiceResponseTime());
		
		globalResponseTime= res1+max1+TestedSC.get(8).getServiceResponseTime()+TestedSC.get(9).getServiceResponseTime();
		
		
		
		   
	    Double r1=(TestedSC.get(0).getServiceReliability()* TestedSC.get(1).getServiceReliability())
	    		+ p23* (TestedSC.get(2).getServiceReliability()* TestedSC.get(3).getServiceReliability())
	    		+p4* TestedSC.get(4).getServiceReliability();
	    Double r2= TestedSC.get(5).getServiceReliability()* TestedSC.get(6).getServiceReliability()*TestedSC.get(7).getServiceReliability();
	    
	    globalReliability= TestedSC.get(8).getServiceReliability()* TestedSC.get(9).getServiceReliability()*r1*r2;
	    		
	    		
	    
	    */
	solution.setObjective(0, -1*globalThroughput);
	solution.setObjective(1,   globalResponseTime);
	solution.setObjective(2, -1* globalReliability);
						
	
}
	}
