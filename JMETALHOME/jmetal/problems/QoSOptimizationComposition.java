package jmetal.problems;



import jmetal.core.Problem;
import jmetal.core.Solution;
import jmetal.encodings.solutionType.BinaryRealSolutionType;
import jmetal.encodings.solutionType.RealSolutionType;
import jmetal.encodings.solutionType.QoS_SOLUTION_TYPE;

import jmetal.util.JMException;
import java.util.ArrayList;

import Core.Service;


public class QoSOptimizationComposition extends Problem{
	
	/**
	 * 
	 */
	//private static final long serialVersionUID = 1L;
	public double globalThroughput = 0;
	public double globalResponseTime = 0;
	public double globalReliability = 0;
	//public double globalCost = 0;
	public double p1 = Math.random();
	public double p2 = Math.random();
	public double p3=Math.random();
	
	
	
	
	public static ArrayList<Service> TestedSC;
	public static ArrayList<Service> TestedSC2;
	
	
	
	public QoSOptimizationComposition(String solutionType) {
	
		numberOfObjectives_ = 3;
		
		numberOfConstraints_ = 0;
		problemName_ = "QoS_COMPOSITION_PROBELM";
		
		
		
		if (solutionType.compareTo("BinaryReal") == 0)
			solutionType_ = new BinaryRealSolutionType(this);
		else if (solutionType.compareTo("Real") == 0)
			solutionType_ = new RealSolutionType(this);
	
		
		
		
		else if (solutionType.compareTo("QoS_SOLUTION_TYPE") == 0)
		{
			solutionType_ = new QoS_SOLUTION_TYPE(this);
		}
		else {
			System.out.println("Error: solution type " + solutionType + " invalid");
			System.exit(-1);
		}
		
	}
	
	
	public void evaluate(Solution solution) throws JMException {
		
		ArrayList<Service> TestedSC=new ArrayList<Service>();
		
		
		
		for (int i=0; i<solution.getCompositionService().size();i++)
		{ 
		
				TestedSC.add(solution.getCompositionService().get(i));

		}
		

		
		   
		//globalThroughput = TestedSC.get(0).getServiceCost() + (p1*TestedSC.get(1).getServiceCost() + p2*TestedSC.get(2).getServiceCost() + p3*TestedSC.get(3).getServiceCost()  );
		Double min1= Math.min(TestedSC.get(1).getServiceThroughput(),TestedSC.get(4).getServiceThroughput());
	    Double min2=Math.min(min1,(Math.min(TestedSC.get(2).getServiceThroughput(),TestedSC.get(3).getServiceThroughput())));	
	    globalThroughput=Math.min(min2,TestedSC.get(0).getServiceThroughput());
	    
	    
	    Double max= Math.max (TestedSC.get(1).getServiceResponseTime()+TestedSC.get(4).getServiceResponseTime(), TestedSC.get(2).getServiceResponseTime());
	    globalResponseTime= TestedSC.get(0).getServiceResponseTime() + Math.max (TestedSC.get(3).getServiceResponseTime(),max);
	   	
	    globalReliability= TestedSC.get(0).getServiceReliability()* (TestedSC.get(1).getServiceReliability()*TestedSC.get(4).getServiceReliability())* TestedSC.get(2).getServiceReliability()*TestedSC.get(3).getServiceReliability();
	
	
	
	solution.setObjective(0, -1*globalThroughput);
	solution.setObjective(1, globalResponseTime);
	solution.setObjective(2, -1*globalReliability);
						
	
}
	}
