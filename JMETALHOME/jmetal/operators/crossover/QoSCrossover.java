//  SinglePointCrossover.java
//
//  Author:
//       Antonio J. Nebro <antonio@lcc.uma.es>
//       Juan J. Durillo <durillo@lcc.uma.es>
//
//  Copyright (c) 2011 Antonio J. Nebro, Juan J. Durillo
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

package jmetal.operators.crossover;

import jmetal.core.Solution;
import jmetal.encodings.solutionType.BinaryRealSolutionType;
import jmetal.encodings.solutionType.BinarySolutionType;
import jmetal.encodings.solutionType.IntSolutionType;
import jmetal.encodings.solutionType.QoS_SOLUTION_TYPE;
import jmetal.encodings.variable.Binary;
import jmetal.util.Configuration;
import jmetal.util.JMException;
import jmetal.util.PseudoRandom;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import Core.Service;

/**
 * This class allows to apply a Single Point crossover operator using two parent
 * solutions.
 */
public class QoSCrossover extends Crossover {
  /**
   * Valid solution types to apply this operator 
   */
protected static Random random = new Random();
	private static final List VALID_TYPES = Arrays.asList(QoS_SOLUTION_TYPE.class,
			QoS_SOLUTION_TYPE.class,
			QoS_SOLUTION_TYPE.class) ;

  private Double crossoverProbability_ = null;
  private int corssoverPoint_=0; 
  private int Solutionlength_=0; 

  /**
   * Constructor
   * Creates a new instance of the single point crossover operator
   */
  public QoSCrossover(HashMap<String, Object> parameters) {
  	super(parameters) ;
  	if (parameters.get("probability") != null)
  		crossoverProbability_ = (Double) parameters.get("probability") ; 
  	
  	crossoverProbability_ = (Double) parameters.get("probability") ;
  //system.out.println("cross over Prob in the QoSCrossover.java class    "+crossoverProbability_);
  	//corssoverPoint_= parameters.get("corssoverPoint") ;
  	
  Solutionlength_=(Integer)parameters.get("Solutionlength") ;
  	//system.out.println("cross over length    "+Solutionlength_);
  } // SinglePointCrossover


  /**
   * Constructor
   * Creates a new instance of the single point crossover operator
   */
  //public SinglePointCrossover(Properties properties) {
  //    this();
  //} // SinglePointCrossover

  /**
   * Perform the crossover operation.
   * @param probability Crossover probability
   * @param parent1 The first parent
   * @param parent2 The second parent   
   * @return An array containig the two offsprings
   * @throws JMException
   */
  public Solution[] doCrossover(double probability,
		  Solution parent1,
          Solution parent2) throws JMException {
	  
	  //system.out.println("I'm in doCrossover methode \n parent1  size: "+ parent1.getCompositionService().size()+" parent2  size: "+ parent2.getCompositionService().size() );
	  
	  
	  
	  //system.out.println("parent1.getCompositionService().size()  "+ parent1.getCompositionService().size());
	  corssoverPoint_= random.nextInt(parent1.getCompositionService().size()-1);//(Math.abs(PseudoRandom.randInt()%parent1.getCompositionService().size()))+1;  
    //system.out.println("corssoverPoint EQUALLLLLL: "+corssoverPoint_);
	  Solution[] offSpring = new Solution[2];
	  //system.out.println(" i'm in QoSCrossover.java 111");
    offSpring[0] = new Solution(parent1);
    //system.out.println(" i'm in QoSCrossover.java 222");
    offSpring[1] = new Solution(parent2);
    //system.out.println(" i'm in QoSCrossover.java 3333");
    ArrayList<Service> ServiceCompostionOne = new  ArrayList<Service>(); // new array list of service 
	  ArrayList<Service> ServiceCompostionTwo = new  ArrayList<Service>();
	  //system.out.println("Finiiiiish");
    try {
    	
      if (PseudoRandom.randDouble() < probability) {
    	  //system.out.println("JUST TO TEST WHERE PROBLEM: "+parent1.getCompositionService().get(1).getServiceId());
    	  //system.out.println("compos111, PseudoRandom.randDouble(): "+ PseudoRandom.randDouble()+ " ,probability: "+probability);
    	  //system.out.println("compos2222");
    	  ServiceCompostionOne=new   ArrayList<Service>();
    	  for (int part1=0;part1<corssoverPoint_;part1++){
    		  //system.out.println("part1  "+ part1);
    	//	  //system.out.println( (parent1.getCompositionService().get(part1).getServiceId()));
    		  //parent1.getCompositionService().get(part1).getServiceId()
    		  //system.out.println("compos333333335AAAAAAAAA333333");
    	// ServiceCompostionOne.set(part1, parent1.getCompositionService().get(part1));
    	 ServiceCompostionOne.add(parent1.getCompositionService().get(part1));
    	 //system.out.println("compos333333333333333333333333333  "+ parent1.getCompositionService().get(part1).getServiceId());
    	 //set(part1, parent1.getCompositionService().get(part1));
     	 
    	  }
    	  //system.out.println("compos88888888: "+corssoverPoint_);
    	  //system.out.println("compos4444:  "+Solutionlength_);
    	  for (int part2=corssoverPoint_;part2<Solutionlength_;part2++){
    	 // for (int part2=corssoverPoint_;part2<parent1.getCompositionService().size();part2++){ 
    	  //ServiceCompostionOne.set(part2, parent2.getCompositionService().get(part2));
    		  
    	  ServiceCompostionOne.add(parent2.getCompositionService().get(part2));
    	  //system.out.println("compos5555");
    		  //system.out.println( "compos6666  "+(parent1.getCompositionService().get(part2).getServiceId()));
    		  //system.out.println("compos7777");
    	  }
    	  
    	  //system.out.println("finish ServiceCompostionOne");
    	 
    	  for (int part1=0;part1<corssoverPoint_;part1++){
    		//  ServiceCompostionTwo.set(part1, parent2.getCompositionService().get(part1));
    		  //system.out.println("compos9999:"+parent2.getCompositionService().get(part1).getServiceId());
    		  ServiceCompostionTwo.add(parent2.getCompositionService().get(part1));
    	    	 
    	  }
   
    	  //system.out.println("i'm in line 152");
    	  for (int part2=corssoverPoint_;part2<Solutionlength_;part2++)
    		//  for (int part2=corssoverPoint_;part2<parent1.getCompositionService().size();part2++)
    	  {
    		  //system.out.println("corssoverPoint_: "+ corssoverPoint_+" Solutionlength_ "+ Solutionlength_ );
    		// ServiceCompostionTwo.set(part2, parent1.getCompositionService().get(part2));
    		 ServiceCompostionTwo.add(parent1.getCompositionService().get(part2));
        	 
    	  }
    	     
    	  // added 9-2-2016
    	  //system.out.println(ServiceCompostionOne.size() + "  drgn " );

    	    //system.out.println(ServiceCompostionTwo.size()+ "   drgn");

    	  offSpring[0].setCompositionService(ServiceCompostionOne);
    	    offSpring[1].setCompositionService(ServiceCompostionTwo); 
// //system.out.println( offSpring[0].getCompositionService().size()+ "+   SIZE 1");
////system.out.println( offSpring[1].getCompositionService().size()+ "+   SIZE 2");
       
      }
   
    } catch (ClassCastException e1) {
      Configuration.logger_.severe("SinglePointCrossover.doCrossover: Cannot perfom " +
              "SinglePointCrossover");
      Class cls = java.lang.String.class;
      String name = cls.getName();
      throw new JMException("Exception in " + name + ".doCrossover()");
    }
       //offSpring[0].setCompositionService(ServiceCompostionOne);
  //  offSpring[1].setCompositionService(ServiceCompostionTwo);
     return offSpring;
  } // doCrossover

  /**
   * Executes the operation
   * @param object An object containing an array of two solutions
   * @return An object containing an array with the offSprings
   * @throws JMException
   */
  public Object execute(Object object) throws JMException {
    Solution[] parents = (Solution[]) object;
    //system.out.println("I'm in QoSCrossover.java in execute method 111 ");
    if (!(VALID_TYPES.contains(parents[0].getType().getClass())  &&
        VALID_TYPES.contains(parents[1].getType().getClass())) ) {
    	//system.out.println("I'm in QoSCrossover.java in execute method 222 ");
      Configuration.logger_.severe("SinglePointCrossover.execute: the solutions " +
              "are not of the right type. The type should be 'Binary' or 'Int', but " +
              parents[0].getType() + " and " +
              parents[1].getType() + " are obtained");
      //system.out.println("I'm in QoSCrossover.java in execute method 333 ");
      Class cls = java.lang.String.class;
      String name = cls.getName();
      throw new JMException("Exception in " + name + ".execute()");
    } // if
//system.out.println("I'm in QoSCrossover.java in execute method 444, and parents size= "+parents.length);
    if (parents.length < 2) {
    	//system.out.println("I'm in QoSCrossover.java in execute method 444");
      Configuration.logger_.severe("SinglePointCrossover.execute: operator " +
              "needs two parents");
      Class cls = java.lang.String.class;
      //system.out.println("I'm in QoSCrossover.java in execute method 555 ");
      String name = cls.getName();
      throw new JMException("Exception in " + name + ".execute()");
    } 
    
    Solution[] offSpring;
    //system.out.println("I'm in QoSCrossover.java and parents[0] type is: "+ parents[0].getType());
    offSpring = doCrossover(crossoverProbability_,
            parents[0],
            parents[1]);
    //system.out.println("I'm in QoSCrossover.java in execute method 111 ");
    ////system.out.println("offffffspring length:  "+offSpring.length);
    //-> Update the offSpring solutions
    for (int i = 0; i < offSpring.length; i++) {
    	//system.out.println("I'm in QoSCrossover.java in execute method 222 ");
      offSpring[i].setCrowdingDistance(0.0);
      offSpring[i].setRank(0);
    }
    return offSpring;
  } // execute
} // SinglePointCrossover
