//  NSGAII_main.java
//
//  Author:
//       Antonio J. Nebro <antonio@lcc.uma.es>
//       Juan J. Durillo <durillo@lcc.uma.es>
//
//  Copyright (c) 2011 Antonio J. Nebro, Juan J. Durillo
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

package jmetal.metaheuristics.nsgaII;

import jmetal.core.Algorithm;
import jmetal.core.Operator;
import jmetal.core.Problem;
import jmetal.core.Solution;
import jmetal.core.SolutionSet;
import jmetal.operators.crossover.CrossoverFactory;
import jmetal.operators.mutation.MutationFactory;
import jmetal.operators.selection.SelectionFactory;
import jmetal.problems.ProblemFactory;
import jmetal.problems.QoSOptimizationComposition;
import jmetal.problems.ZDT.ZDT3;
import jmetal.qualityIndicator.QualityIndicator;
import jmetal.util.Configuration;
import jmetal.util.JMException;
import jmetal.util.PseudoRandom;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;
import java.util.logging.FileHandler;
import java.util.logging.Logger;

import Core.Service;
import Core.ServiceClass;
import Core.ServicesReader;

/** 
 * Class to configure and execute the NSGA-II algorithm.  
 *     
 * Besides the classic NSGA-II, a steady-state version (ssNSGAII) is also
 * included (See: J.J. Durillo, A.J. Nebro, F. Luna and E. Alba 
 *                  "On the Effect of the Steady-State Selection Scheme in 
 *                  Multi-Objective Genetic Algorithms"
 *                  5th International Conference, EMO 2009, pp: 183-197. 
 *                  April 2009)
 */ 

public class NSGAII_main {
	
	
	public static Logger      logger_ ;     
    public static ServicesReader Chromo=new ServicesReader();
	public static ArrayList<Service> BasicChmomsome =new ArrayList<Service>();
	public static ArrayList<ServiceClass> classes=Chromo.ClassPool;
	public static ArrayList<Service> AllServicePool=Chromo.ServicePool;
	public static	ArrayList<Service> TempClass =new ArrayList<Service>();
   public static FileHandler fileHandler_ ; 
  
  

  /**
   * @param args Command line arguments.
   * @throws JMException 
   * @throws IOException 
   * @throws SecurityException 
   * Usage: three options
   *      - jmetal.metaheuristics.nsgaII.NSGAII_main
   *      - jmetal.metaheuristics.nsgaII.NSGAII_main problemName
   *      - jmetal.metaheuristics.nsgaII.NSGAII_main problemName paretoFrontFile
   */
  public static void main(String [] args) throws 
                                  JMException, 
                                  SecurityException, 
                                  IOException, 
                                  ClassNotFoundException {
	
    Problem   problem   ;
    Algorithm algorithm ; 
    Operator  crossover ; 
    Operator  mutation  ;
    Operator  selection ; 
   
    HashMap  parameters ; 
    
    QualityIndicator indicators ; 

    // Logger object and file to store log messages
    logger_      = Configuration.logger_ ;
    fileHandler_ = new FileHandler("NSGAII_main.log"); 
    logger_.addHandler(fileHandler_) ;
        
    indicators = null ;
    if (args.length == 1) {
      Object [] params = {"Real"};
      problem = (new ProblemFactory()).getProblem(args[0],params);
    } // if
    else if (args.length == 2) {
      Object [] params = {"Real"};
      problem = (new ProblemFactory()).getProblem(args[0],params);
      indicators = new QualityIndicator(problem, args[1]) ;
    } // if
    else { 
    	
    	Chromo.ReadServices();
    
    	Chromo.ReadServiceClass();
    	
    	logger_.info("Hiiii");
    	problem=new QoSOptimizationComposition("QoS_SOLUTION_TYPE");
   
    	createBasic(); 
    	
    			
     algorithm = new NSGAII(problem);
     algorithm.setInputParameter("populationSize",100);
     algorithm.setInputParameter("maxEvaluations",25000);
     algorithm.setInputParameter("ServicePool", AllServicePool);
     algorithm.setInputParameter("ClassPool", classes);
     algorithm.setInputParameter("BasicSolution",BasicChmomsome);
    
    parameters = new HashMap() ;
    parameters.put("probability", 0.9) ;
    parameters.put("distributionIndex", 20.0) ;
  
    parameters = new HashMap() ;
    parameters.put("probability", 0.9) ;
    
    parameters.put("distributionIndex", 20.0) ;
    parameters.put("Solutionlength", 6) ;
    crossover = CrossoverFactory.getCrossoverOperator("QoSCrossover", parameters);              
    mutation = MutationFactory.getMutationOperator("QoS_BitFlipMutation", parameters); 
    parameters = null ;
    selection = SelectionFactory.getSelectionOperator("BinaryTournament2", parameters) ;                           

    // Add the operators to the algorithm
    algorithm.addOperator("crossover",crossover);
    algorithm.addOperator("mutation",mutation);
    algorithm.addOperator("selection",selection);

    // Add the indicator object to the algorithm
    algorithm.setInputParameter("indicators", indicators) ;
    
    // Execute the Algorithm
    long initTime = System.currentTimeMillis();
    SolutionSet population = algorithm.execute();
   
    long estimatedTime = System.currentTimeMillis() - initTime;
    // Result messages 
    logger_.info("ttttTotal execution time: "+estimatedTime + "ms");
    logger_.info("Objectives values have been writen to file FUN");
    population.printObjectivesToFile("FUN");
    population.printVariablesToFile("VAR");  
    logger_.info("Variables values have been writen to file VAR");
     
  if (indicators != null) {
	     logger_.info("Quality indicators") ;
	      logger_.info("Hypervolume: " + indicators.getHypervolume(population)) ;
	      logger_.info("GD         : " + indicators.getGD(population)) ;
	     logger_.info("IGD        : " + indicators.getIGD(population)) ;
	      logger_.info("Spread     : " + indicators.getSpread(population)) ;
	     logger_.info("Epsilon    : " + indicators.getEpsilon(population)) ;  
	     
	       int evaluations = ((Integer)algorithm.getOutputParameter("evaluations")).intValue();
	     logger_.info("Speed      : " + evaluations + " evaluations") ;      
	     } // if
	    }
	  } //main

private static void createBasic() {
	Random random = new Random();
	BasicChmomsome=new ArrayList<Service>();
	int j=0;
	for (int i=0; i<= classes.size()-1; i++){
		fillarray(classes.get(i));
		j = random.nextInt(TempClass.size()-1);
		
		BasicChmomsome.add(TempClass.get(j));
		TempClass =new ArrayList<Service>();
		
	}
	

	
}
private static void fillarray(ServiceClass class1) {
	// TODO Auto-generated method stub
	
	
	for (int i=0;i<AllServicePool.size();i++){if (AllServicePool.get(i).getServiceClass().equals(class1.getClassid()))    
		{
		TempClass.add(AllServicePool.get(i));
		}
		}
	
}


}  // NSGAII_main
