//  NSGAII.java
//
//  Author:
//       Antonio J. Nebro <antonio@lcc.uma.es>
//       Juan J. Durillo <durillo@lcc.uma.es>
//
//  Copyright (c) 2011 Antonio J. Nebro, Juan J. Durillo
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

package jmetal.metaheuristics.nsgaII;

import java.util.ArrayList;
import java.util.Random;

import Core.Service;
import Core.ServiceClass;
import edu.birzeit.jmse.sbse.FileUtils;
import jmetal.core.*;
import jmetal.qualityIndicator.Hypervolume;
import jmetal.qualityIndicator.QualityIndicator;
import jmetal.util.Distance;
import jmetal.util.JMException;
import jmetal.util.Ranking;
import jmetal.util.comparators.CrowdingComparator;

/** 
 *  Implementation of NSGA-II.
 *  This implementation of NSGA-II makes use of a QualityIndicator object
 *  to obtained the convergence speed of the algorithm. This version is used
 *  in the paper:
 *     A.J. Nebro, J.J. Durillo, C.A. Coello Coello, F. Luna, E. Alba 
 *     "A Study of Convergence Speed in Multi-Objective Metaheuristics." 
 *     To be presented in: PPSN'08. Dortmund. September 2008.
 */

public class NSGAII extends Algorithm {
	
	  private Variable[] variable_ ;
	 private SolutionType type_ ; 
	 
	 
	public static long[] periodBase;
	 static long[] period;
	 String hvString = "";
	int periodIndex = 0;
	//double[] hvArray = new double[periodBase.length];
	// $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
	public static double[][] trueFront = {};
	
	  static Random random = new Random();
	private ArrayList<Service> basicChmomsome;
	public static String problemName= "";
	public static String problemName_;
  /**
   * Constructor
   * @param problem Problem to solve
   */
  public NSGAII(Problem problem) {
    super (problem) ;
  } // NSGAII

  /**   
   * Runs the NSGA-II algorithm.
   * @return a <code>SolutionSet</code> that is a set of non dominated solutions
   * as a result of the algorithm execution
   * @throws JMException 
   */
  public SolutionSet execute() throws JMException, ClassNotFoundException {
    int populationSize;
    int maxEvaluations;
    int evaluations;
    ArrayList<Service> BasicChmomsome ;
    ArrayList<Service> Spool;
    ArrayList<ServiceClass> classes; 
   System.out.println("333333333333333");
    QualityIndicator indicators; // QualityIndicator object
    int requiredEvaluations; // Use in the example of use of the
    // indicators object (see below)
  //  System.out.println("4444444444444");
    SolutionSet population;
    SolutionSet offspringPopulation;
    SolutionSet union;

    Operator mutationOperator;
    Operator crossoverOperator;
    Operator selectionOperator;

    Distance distance = new Distance();
   // System.out.println("55555555555555555555555");
    //Read the parameters
    populationSize = ((Integer) getInputParameter("populationSize")).intValue();
   // System.out.println("populationSize:"+ populationSize);
 //   System.out.println("pooooooop");
    maxEvaluations = ((Integer) getInputParameter("maxEvaluations")).intValue();
 //   System.out.println("maxEvaluations:"+ maxEvaluations);
  //  System.out.println("maaaaaaaaaaaaax");
    indicators = (QualityIndicator) getInputParameter("indicators");
  //  System.out.println("indicators:"+ indicators);
    
    //$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
 //   System.out.println("6666666666666666");
	BasicChmomsome=(ArrayList)getInputParameter("BasicSolution");
	// System.out.println("BasicChmomsome:"+ BasicChmomsome);
	//System.out.println("siiiiize of basic chromosome: "+BasicChmomsome.get(0));
//	System.out.println("77777777777");
    Spool = (ArrayList<Service>) getInputParameter("ServicePool");
 //   System.out.println("888888888888888888");
   // System.out.println("SPOOOOL: "+ Spool);
   // for(int s=0; s< Spool.size(); s++){
    //	System.out.println("in execute inside NSGAII.java, Sppol[i]: "+ Spool.get(s));
    //}
   // System.out.println("read services parameters are readed");
    classes=(ArrayList<ServiceClass>) getInputParameter("ClassPool");
 //   System.out.println("99999999999999999999999");
   // System.out.println("read classes ");
    //Initialize the variables


    
    
    //Initialize the variables
    population = new SolutionSet(populationSize);
    evaluations = 0;
   // System.out.println("10101010");
    requiredEvaluations = 0;


    //Read the operators
    mutationOperator = operators_.get("mutation");
    crossoverOperator = operators_.get("crossover");
    selectionOperator = operators_.get("selection");
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
   // BasicChmomsome=(ArrayList)getInputParameter("BasicSolution");
   // System.out.println("c1111111111111");
  //  System.out.println("populationSize "+populationSize);
  
    // Create the initial solutionSet
    Solution newSolution;
  //  System.out.println("newSolution typereee: ");
    for (int i = 0; i < populationSize; i++) {
    	//System.out.println( "this.problem_: "+this.problem_ );
    //  newSolution = new Solution(Spool, classes,problem_);
   // System.out.println("newSolution type1: "+newSolution);

    	newSolution = new Solution(problem_);
    //	System.out.println("newSolution type1: ");
         
    	newSolution.setCompositionService(getMyServices(Spool, classes));
    //   System.out.println("newSolution type2: ");
       //getCompositionService().get(ii).getServiceId()
      //$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
      
      //Here b3bii chromosome feeh services with solution size
      
      /*
      newSolution.setCompositionService(getMyServices(Spool, classes));
    //  System.out.println("newSolution type2: "+newSolution);
	   
      
      for (int ii = 0; ii < newSolution.getCompositionService().size(); ii++) {
    	  System.out.println("Print popNSGA: "+ii+"pop siz: "+i );
	    	System.out.println("NSGASolution "+newSolution.getCompositionService().get(ii).getServiceId());	
      	    }
      
      type_=newSolution.getType();
      System.out.println("NSGA|| type_: "+type_);
      */
      //System.out.println("evaaaAAte");
    
     // type_ = problem_.getSolutionType() ;
     // variable_=new Variable[];
   //   variable_ = type_.createVariables(newSolution,Spool, classes,problem_) ; 
      
      
   //  System.out.println("evaaaaaaaaaaaluate");
      problem_.evaluate(newSolution);
    //  problem_.evaluateConstraints(newSolution);
      evaluations++;
      population.add(newSolution);
     // System.out.println("evaluationnn NOOO: "+ evaluations);
    } //for       
   // System.out.println("101010102222222222222222222");
   // Solution s1=new Solution(); 
    
    /*
    for (int i = 0; i < populationSize; i++) {
    	System.out.println(populationSize   +" popppp");
    	    for(int x=0;x<population.get(i).getCompositionService().size();x++){ 
    	   	System.out.println("Print pop " );
    	    	System.out.println("Solution "+i+" Service "+x+ "_"+population.get(i).getCompositionService().get(x).getServiceId());
		    	    	
    	    }
    	    	
    	    }
    	    	
    	    	*/
    
    // Generations 
    while (evaluations < maxEvaluations) {
    //	System.out.println("101010103333333");
      // Create the offSpring solutionSet      
      offspringPopulation = new SolutionSet(populationSize);
      Solution[] parents = new Solution[2];
      for (int i = 0; i < (populationSize / 2); i++) {
        if (evaluations < maxEvaluations) {
          //obtain parents
          parents[0] = (Solution) selectionOperator.execute(population);
          //System.out.println("02");
          //  System.out.println("in NSGAII.java in line 156");
        // System.out.println("Size for parents[0]:  "+ parents[0]);
          parents[1] = (Solution) selectionOperator.execute(population);
         // System.out.println("in NSGAII.java in line 158");
          Solution[] offSpring = (Solution[]) crossoverOperator.execute(parents);
         // System.out.println("in NSGAII.java in line 160");
          //$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$4
         // System.out.println("beeeeb11   "+offSpring[0].getCompositionService().size()+"       size 1  ");

          //System.out.println("beeeeb22   "+offSpring[1].getCompositionService().size()+"       size 2  ");
          
       //   System.out.println("MUTEEEEEEEEEE");
          
          
          
          mutationOperator.execute(offSpring[0]);
          mutationOperator.execute(offSpring[1]);
          
          //$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
       //   for (int e=0;e<offSpring[0].getCompositionService().size();e++)
       	 //  System.out.println("  1rst offsbring After mutation"+  offSpring[0].getCompositionService().get(e).getServiceId());
        //  for (int e=0;e<offSpring[1].getCompositionService().size();e++)
       	 //  System.out.println("2nd offsbring After mutation"+  offSpring[1].getCompositionService().get(e).getServiceId());
       //   System.out.println("finish cross over  " );
           
          
          problem_.evaluate(offSpring[0]);
          //problem_.evaluateConstraints(offSpring[0]);
          problem_.evaluate(offSpring[1]);
         // problem_.evaluateConstraints(offSpring[1]);
          offspringPopulation.add(offSpring[0]);
          offspringPopulation.add(offSpring[1]);
          evaluations += 2;
        //  isStillEvaluating(population);
        } // if                            
      } // for

      // Create the solutionSet union of solutionSet and offSpring
      union = ((SolutionSet) population).union(offspringPopulation);

      // Ranking the union
      Ranking ranking = new Ranking(union);

      int remain = populationSize;
      int index = 0;
      SolutionSet front = null;
      population.clear();

      // Obtain the next front
      front = ranking.getSubfront(index);

      while ((remain > 0) && (remain >= front.size())) {
        //Assign crowding distance to individuals
        distance.crowdingDistanceAssignment(front, problem_.getNumberOfObjectives());
        //Add the individuals of this front
        for (int k = 0; k < front.size(); k++) {
          population.add(front.get(k));
        } // for

        //Decrement remain
        remain = remain - front.size();

        //Obtain the next front
        index++;
        if (remain > 0) {
          front = ranking.getSubfront(index);
        } // if        
      } // while

      // Remain is less than front(index).size, insert only the best one
      if (remain > 0) {  // front contains individuals to insert                        
        distance.crowdingDistanceAssignment(front, problem_.getNumberOfObjectives());
        front.sort(new CrowdingComparator());
        for (int k = 0; k < remain; k++) {
          population.add(front.get(k));
        } // for

        remain = 0;
      } // if                               

      // This piece of code shows how to use the indicator object into the code
      // of NSGA-II. In particular, it finds the number of evaluations required
      // by the algorithm to obtain a Pareto front with a hypervolume higher
      // than the hypervolume of the true Pareto front.
      if ((indicators != null) &&
          (requiredEvaluations == 0)) {
        double HV = indicators.getHypervolume(population);
        if (HV >= (0.98 * indicators.getTrueParetoFrontHypervolume())) {
          requiredEvaluations = evaluations;
        } // if
      } // if
    } // while

    
    // Return as output parameter the required evaluations
    setOutputParameter("evaluations", requiredEvaluations);

    // Return the first non-dominated front
    Ranking ranking = new Ranking(population);
//    System.out.println("NSGAII line 268");
    ranking.getSubfront(0).printFeasibleFUN("FUN_NSGAII") ;

    /*
    for (int i = 0; i < populationSize; i++) {
    	System.out.println(populationSize   +" popppp");
    	    for(int x=0;x<population.get(i).getCompositionService().size();x++){ 
    	   	System.out.println("Print pop " );
    	    	System.out.println("Solution "+i+" Service "+x+ "_"+population.get(i).getCompositionService().get(x).getServiceId());
		    	    	
    	    
    	    }
    	    	
    	    }
    */
    
 //   System.out.println(ranking.getSubfront(0).get(99).getCompositionService().get(2).getServiceId()   +" raaank");
   // System.out.println(ranking.getSubfront(0).get(99).getCompositionService().get(0).getServiceId()   +" raaank");
    
 //   System.out.println(ranking.getSubfront(0).get(99).getCompositionService().get(1).getServiceId()   +" raaank");
    
    return ranking.getSubfront(0);
  } // execute
  
  
  
  /*
  private void isStillEvaluating(SolutionSet population){
  
  getHv(population);
	periodIndex++;
	if (periodIndex >= hvArray.length) {
		for (int i = 0; i < hvArray.length; i++) {
			hvString += hvArray[i] + "\t";
		}
		FileUtils.appendFile(hvString, "HVresults_" + problemName);
	
	}
  
  }
  
  
  
  private void getHv(SolutionSet population) {
		
		Ranking tempRanking = new Ranking(population);
		tempRanking.getSubfront(0).printFeasibleFUN("FUN_NSGAII");

		double[][] paretoFront = tempRanking.getSubfront(0).writeObjectivesToMatrix();

		Hypervolume hvByTime = new Hypervolume();

		// System.out.println( this.problem_ );
		double hv = hvByTime.hypervolume(paretoFront, trueFront, trueFront[0].length);
		hvArray[periodIndex] = hv;

	}
  
  
  
  */
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
 //$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
  private static ArrayList<Service> fillarray(ServiceClass class1, ArrayList<Service> AllServicePool) {
	  ArrayList<Service> SolutionTempClass =new ArrayList<Service>();
	  
	  for (int i=0;i<AllServicePool.size();i++){
		  String serviceClass= class1.getClassid();
		if (AllServicePool.get(i).getServiceClass().equals(serviceClass))
			SolutionTempClass.add(AllServicePool.get(i));
		//System.out.println("Class temp  fill " );
		}
		return SolutionTempClass;
	}

  public ArrayList<Service> getMyServices( ArrayList<Service> AllServicePool, ArrayList<ServiceClass> classes){
	
	  ArrayList<Service> SoutionServices = new ArrayList<Service> ();
	  ArrayList<Service>TempClass; 
	  //ServiceClass class1
//	 System.out.println("get my classes "+ classes.size());

	// System.out.println("get my services "+ AllServicePool.size());
	    	for (int i=0; i< classes.size(); i++){
	    		 TempClass =new ArrayList<Service>();  

	    		// System.out.println("Step 1");
	    		TempClass=fillarray(classes.get(i), AllServicePool);
	    		//System.out.println("call fill array " );
		//BasicChmomsome.add();
	//	Solution 	BasicChomosome = new Solution();
		
		
		
		//Service service;

		//BasicChmomsome.add(TempClass.get(j));
		
	  
	 // System.out.println("Step 1");
	  
	  //ArrayList<Service> SoutionServicesClassA=new ArrayList<Service>();
	  //ArrayList<Service> SoutionServicesClassB=new ArrayList<Service>();
	  //ArrayList<Service> SoutionServicesClassC=new ArrayList<Service>();
	  //ArrayList<Service> SoutionServicesClassD=new ArrayList<Service>();
	  
	/*  for(int s=0;s<AllServicePool.size();s++){
		  
		  System.out.println("Step 2");
		  
		  
		  System.out.println( AllServicePool.get(s).getSeviceClass());
		if(AllServicePool.get(s).getSeviceClass().equals("A")){
			SoutionServicesClassA.add(AllServicePool.get(s));
			
		}
		
		if(AllServicePool.get(s).getSeviceClass().equals("B")){
			SoutionServicesClassB.add(AllServicePool.get(s));
			
		}
		
		if(AllServicePool.get(s).getSeviceClass().equals("C")){
			SoutionServicesClassC.add(AllServicePool.get(s));
			
		}
		
		if(AllServicePool.get(s).getSeviceClass().equals("D")){
			SoutionServicesClassD.add(AllServicePool.get(s));
			
		}
		
		
		  
	  }
*/	  
	  
	  //System.out.println("SoutionServicesClassA.size()" +SoutionServicesClassA.size());
	int RandomVal=random.nextInt(TempClass.size()-1);
//	System.out.println("RandomVal"+RandomVal);
	SoutionServices.add(TempClass.get(RandomVal));
	//System.out.println("add from temp class " );
	// System.out.println(SoutionServicesClassB.size());
	
 //RandomVal= random.nextInt(SoutionServicesClassB.size()-1);
	//System.out.println("RandomVal"+RandomVal);
	//SoutionServices.add(SoutionServicesClassB.get(RandomVal));
	 
	 //System.out.println(SoutionServicesClassC.size());
	 //RandomVal= random.nextInt(SoutionServicesClassC.size()-1);
		
		//SoutionServices.add(SoutionServicesClassC.get(RandomVal));
		//System.out.println("RandomVal"+RandomVal);
		/* RandomVal= random.nextInt(SoutionServicesClassD.size());
			
			SoutionServices.add(SoutionServicesClassD.get(RandomVal));*/
	 
	 // return SoutionServices;
	
	    	}
	    //	System.out.println(SoutionServices.size()+ "SoutionServices    SIZE IN NSGAII ");
  return SoutionServices;
  }
  
  
  
  
  
  
} // NSGA-II
