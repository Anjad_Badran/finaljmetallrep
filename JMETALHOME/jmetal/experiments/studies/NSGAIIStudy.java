//  NSGAIIStudy.java
//
//  Authors:
//       Antonio J. Nebro <antonio@lcc.uma.es>
//       Juan J. Durillo <durillo@lcc.uma.es>
//
//  Copyright (c) 2011 Antonio J. Nebro, Juan J. Durillo
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

package jmetal.experiments.studies;

import jmetal.core.Algorithm;
import jmetal.experiments.Experiment;
import jmetal.experiments.Settings;
import jmetal.experiments.settings.NSGAII_Settings;
import jmetal.experiments.util.Friedman;
import jmetal.qualityIndicator.Hypervolume;
import jmetal.qualityIndicator.util.MetricsUtil;
import jmetal.util.JMException;
import jmetal.util.NonDominatedSolutionList;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import edu.birzeit.jmse.sbse.FileUtils;
import edu.birzeit.jmse.sbse.reporting.RadarChartGenerator;

/**
 * Class implementing an example of experiment using NSGA-II as base algorithm.
 * The experiment consisting in studying the effect of the crossover probability
 * in NSGA-II.
 */

public class NSGAIIStudy extends Experiment {
 
	private static int stopTime = -1;
	//private static int runs = 10;
	/**
   * Configures the algorithms in each independent run
   * @param problemName The problem to solve
   * @param problemIndex
   * @param algorithm Array containing the algorithms to run
   * @throws ClassNotFoundException 
   */
  public synchronized void algorithmSettings(String problemName, 
  		                                       int problemIndex, 
  		                                       Algorithm[] algorithm) 
 
    throws ClassNotFoundException {  	
	  System.out.println("algorithmSettings1");
  	try {
      int numberOfAlgorithms = algorithmNameList_.length;

      HashMap[] parameters = new HashMap[numberOfAlgorithms];

      for (int i = 0; i < numberOfAlgorithms; i++) {
        parameters[i] = new HashMap();
      } // for

      if (!paretoFrontFile_[problemIndex].equals("")) {
    	  System.out.println("algorithmSettings2");
        for (int i = 0; i < numberOfAlgorithms; i++)
          parameters[i].put("paretoFrontFile_", paretoFrontFile_[problemIndex]);
      } // if

  //    parameters[0].put("crossoverProbability_", 1.0);
    //  parameters[1].put("crossoverProbability_", 0.9);
      //parameters[2].put("crossoverProbability_", 0.8);
      //parameters[3].put("crossoverProbability_", 0.7); 
      
      for (int i = 0; i < numberOfAlgorithms; i++) {
			parameters[i].put("crossoverProbability_", 0.9);
		}
		
		
      

      if ((!paretoFrontFile_[problemIndex].equals("")) || 
      		(paretoFrontFile_[problemIndex] == null)) {
        for (int i = 0; i < numberOfAlgorithms; i++)
          parameters[i].put("paretoFrontFile_",  paretoFrontFile_[problemIndex]);
      } // if
 
      
      NSGAII_Settings nsgaiiSettings = new NSGAII_Settings(problemName);
		
     for (int i = 0; i < numberOfAlgorithms; i++){
      System.out.println("algorithmSettings3");
        algorithm[i] = nsgaiiSettings.configure(parameters[i]);
      }
      
     // NSGAII_Settings nsgaiiSettings = new NSGAII_Settings(problemName);
	//	algorithm[0] = nsgaiiSettings.configure(parameters[0]);

      
      
      
    } catch (IllegalArgumentException ex) {
    	Logger.getLogger(NSGAIIStudy.class.getName()).log(Level.SEVERE, null, ex);
    } catch (IllegalAccessException ex) {
      Logger.getLogger(NSGAIIStudy.class.getName()).log(Level.SEVERE, null, ex);
    } catch (JMException ex) {
      Logger.getLogger(NSGAIIStudy.class.getName()).log(Level.SEVERE, null, ex);
    }
  } // algorithmSettings
  
  public static void main(String[] args) throws JMException, IOException {
    NSGAIIStudy exp = new NSGAIIStudy() ; // exp = experiment
    
    exp.experimentName_  = "NSGAIIStudy" ;
    exp.algorithmNameList_   = new String[] {"NSGAII"} ;
    exp.problemList_     = new String[] {"QoSOptimizationComposition"} ;
    
    exp.paretoFrontFile_ = new String[] { "QoSOptimizationComposition.pf"};

	// exp.problemList_ = new String[] { "JDTMilestone 3.1",
	// "JDTMilestone3.1.1", "JDTMilestone3.1.2" };

	// exp.paretoFrontFile_ = new String[] { "JDTMilestone 3.1.pf",
	// "JDTMilestone3.1.1.pf", "JDTMilestone3.1.2.pf" };
    
   // exp.indicatorList_   = new String[] {"HV", "SPREAD", "IGD", "EPSILON"} ;
    exp.indicatorList_ = new String[] { "HV"/* , "SPREAD", "IGD", "EPSILON" */ };
    int numberOfAlgorithms = exp.algorithmNameList_.length ;

	exp.experimentBaseDirectory_ = "StudyOutput/" + exp.experimentName_;
	exp.paretoFrontDirectory_ = "StudyOutput/paretoFronts";

	exp.algorithmSettings_ = new Settings[numberOfAlgorithms];

	exp.independentRuns_ = 10;

	exp.initExperiment();
	
	

    // Run the experiments
    int numberOfThreads =1;
    exp.runExperiment(numberOfThreads) ;

    exp.generateQualityIndicators() ;
    
    // Generate latex tables (comment this sentence is not desired)
    exp.generateLatexTables() ;
    
    // Configure the R scripts to be generated
    int rows  ;
    int columns  ;
    String prefix ;
    String [] problems ;

    rows = 2 ;
    columns = 3 ;
    prefix = new String("Problems");
	problems = new String[] { "QoSOptimizationComposition" };

    boolean notch ;
    exp.generateRBoxplotScripts(rows, columns, problems, prefix, notch = true, exp) ;
    exp.generateRWilcoxonScripts(problems, prefix, exp) ;

    // Applying Friedman test
    Friedman test = new Friedman(exp);
   // test.executeTest("EPSILON");
    test.executeTest("HV");
   // test.executeTest("SPREAD");
    
    
  //  RadarChartGenerator.addSolutionChart("Desktop"
	//		+ "QoSOptimizationComposition" + "_"  + "/data/NSGAII/" + "QoSOptimizationComposition" + "/");
 
    
  } // main
   
  
  public static void runAProblem(String problemName, String developersFile, int stopTimeinSec)
			throws JMException, IOException {
		stopTime = stopTimeinSec;
		NSGAIIStudy exp = new NSGAIIStudy(); // exp = experiment
		
		exp.experimentName_ = problemName + "_" + stopTimeinSec;
		exp.algorithmNameList_ = new String[] { "NSGAII" };// , "IBEA", "MOCell"
															// };

		exp.problemList_ = new String[] { problemName };// , "JDTMilestoneM3" ,
														// "JDTMilestoneM4",
														// "JDTMilestoneM5" };

		exp.paretoFrontFile_ = new String[] { problemName + ".pf" };  

		// exp.problemList_ = new String[] { "JDTMilestone 3.1",
		// "JDTMilestone3.1.1", "JDTMilestone3.1.2" };

		// exp.paretoFrontFile_ = new String[] { "JDTMilestone 3.1.pf",
		// "JDTMilestone3.1.1.pf", "JDTMilestone3.1.2.pf" };

		exp.indicatorList_ = new String[] { "HV"/* , "SPREAD", "IGD", "EPSILON" */ };

		int numberOfAlgorithms = exp.algorithmNameList_.length;

		exp.experimentBaseDirectory_ = "StudyOutput/" + exp.experimentName_;
		exp.paretoFrontDirectory_ = "StudyOutput/paretoFronts";

		exp.algorithmSettings_ = new Settings[numberOfAlgorithms];

		exp.independentRuns_ = 10;

		exp.initExperiment();

		// Run the experiments
		int numberOfThreads = 1;
		exp.runExperiment(numberOfThreads);

		exp.generateQualityIndicators();

		// Generate latex tables (comment this sentence is not desired)
		exp.generateLatexTables();

		// Configure the R scripts to be generated int rows; int columns;
		String prefix;
		String[] problems;

		int rows;
		int columns;

		rows = 2;
		columns = 3;
		prefix = new String("Problems");
		problems = new String[] { problemName };

		boolean notch;
		exp.generateRBoxplotScripts(rows, columns, problems, prefix, notch = true, exp);
		exp.generateRWilcoxonScripts(problems, prefix, exp);

		// Applying Friedman
		Friedman test = new Friedman(exp);
		// test.executeTest("EPSILON");
		test.executeTest("HV");
		// test.executeTest("SPREAD");
		RadarChartGenerator
				.addSolutionChart("/Desktop/"
						+ problemName + "_" + stopTimeinSec + "/data/NSGAII/" + problemName + "/");

	}
  
  
 
  
  public void generateReferenceFronts(int problemIndex) {

		File rfDirectory;
		String referenceFrontDirectory = experimentBaseDirectory_ + "/referenceFronts";
		System.out.println("generateReferenceFronts()1 in Exp NSGAIIStudy");
		rfDirectory = new File(referenceFrontDirectory);

		if (!rfDirectory.exists()) { // Si no existe el directorio
			System.out.println("generateReferenceFronts()2 in Exp NSGAIIStudy");
			boolean result = new File(referenceFrontDirectory).mkdirs(); // Lo creamos
			System.out.println("Creating " + referenceFrontDirectory);
		}

		frontPath_[problemIndex] = referenceFrontDirectory + "/" + problemList_[problemIndex] + ".rf";
		System.out.println("generateReferenceFronts()3 in Exp NSGAIIStudy");
		MetricsUtil metricsUtils = new MetricsUtil();
		NonDominatedSolutionList solutionSet = new NonDominatedSolutionList();
		for (String anAlgorithmNameList_ : algorithmNameList_) {

			String problemDirectory = experimentBaseDirectory_ + "/data/" + anAlgorithmNameList_ + "/"
					+ problemList_[problemIndex];
	//		System.out.println("generateReferenceFronts()4 in Exp NSGAIIStudy");
			for (int numRun = 0; numRun < independentRuns_; numRun++) {

				String outputParetoFrontFilePath;
				outputParetoFrontFilePath = problemDirectory + "/FUN." + numRun;
				String solutionFrontFile = outputParetoFrontFilePath;
		//		System.out.println("generateReferenceFronts()4 in Exp NSGAIIStudy");
				metricsUtils.readNonDominatedSolutionSet(solutionFrontFile, solutionSet);
			} // for
		} // for
		
		// From HrResourcesStudy - Bug Experiment
	//	System.out.println("generateReferenceFronts()5 in Exp NSGAIIStudy");
		String pf = "";
		pf += "-10 500000 -20\n0 0 0";
		FileUtils.writeStringToFile(pf, frontPath_[problemIndex]);
		System.out.println("PFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPF");
		// solutionSet.printObjectivesToFile(frontPath_[problemIndex]);
	} // generateReferenceFronts

  
  
  
  
  public static double generateHVIndicators(String paretoFrontPath, String solutionFrontFile) {
System.out.println("generateHVIndicators111222222233344");
		double[][] trueFront = new Hypervolume().utils_.readFront(paretoFrontPath);
		Hypervolume indicators = new Hypervolume();
		double[][] solutionFront = indicators.utils_.readFront(solutionFrontFile);
		// double[][] trueFront =
		// indicators.utils_.readFront(paretoFrontPath);
		return indicators.hypervolume(solutionFront, trueFront, trueFront[0].length);

	}

  
} // NSGAIIStudy


