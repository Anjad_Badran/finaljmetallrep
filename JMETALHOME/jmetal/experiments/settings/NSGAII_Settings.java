//  NSGAII_Settings.java 
//
//  Authors:
//       Antonio J. Nebro <antonio@lcc.uma.es>
//       Juan J. Durillo <durillo@lcc.uma.es>
//
//  Copyright (c) 2011 Antonio J. Nebro, Juan J. Durillo
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

package jmetal.experiments.settings;

import jmetal.core.Algorithm;
import jmetal.experiments.Settings;
import jmetal.metaheuristics.nsgaII.NSGAII;
import jmetal.operators.crossover.Crossover;
import jmetal.operators.crossover.CrossoverFactory;
import jmetal.operators.mutation.Mutation;
import jmetal.operators.mutation.MutationFactory;
import jmetal.operators.selection.Selection;
import jmetal.operators.selection.SelectionFactory;
import jmetal.problems.ProblemFactory;
import jmetal.util.JMException;

import java.util.HashMap;
import java.util.Properties;

import jmetal.core.Operator;
import jmetal.core.Problem;
import jmetal.core.Solution;
import jmetal.core.SolutionSet;
import jmetal.operators.crossover.CrossoverFactory;
import jmetal.problems.QoSOptimizationComposition;
import jmetal.problems.ZDT.ZDT3;
import jmetal.qualityIndicator.QualityIndicator;
import jmetal.util.Configuration;
import jmetal.util.JMException;
import jmetal.util.PseudoRandom;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;
import java.util.logging.FileHandler;
import java.util.logging.Logger;

import Core.Service;
import Core.ServiceClass;
import Core.ServicesReader;

/**
 * Settings class of algorithm NSGA-II (real encoding)
 */
public class NSGAII_Settings extends Settings {
  public int populationSize_                 ;
  public int maxEvaluations_                 ;
  public double mutationProbability_         ;
  public double crossoverProbability_        ;
  public double mutationDistributionIndex_   ;
  public double crossoverDistributionIndex_  ;
  
  public static Logger      logger_ ;     
  public static ServicesReader Chromo=new ServicesReader();
	public static ArrayList<Service> BasicChmomsome =new ArrayList<Service>();
	public static ArrayList<ServiceClass> classes=Chromo.ClassPool;
	public static ArrayList<Service> AllServicePool=Chromo.ServicePool;
	public static	ArrayList<Service> TempClass =new ArrayList<Service>();
 public static FileHandler fileHandler_ ; 


  /**
   * Constructor
   */
  public NSGAII_Settings(String problem) {
	 
    super(problem) ;
    //System.out.println("NSGAII_Settings1");
    
    Chromo.ReadServices();
    
	Chromo.ReadServiceClass();
	
	// System.out.println("problem_11");
	 
	problem_=new QoSOptimizationComposition("QoS_SOLUTION_TYPE");
	// System.out.println("problem_22");
	createBasic(); 
	
    
    

  //  Object [] problemParams = {"Real"};
   // try {
//	    problem_ = (new ProblemFactory()).getProblem(problemName_, problemParams);
 //   } catch (JMException e) {
	    // TODO Auto-generated catch block
//	    e.printStackTrace();
 //   }
    // Default experiments.settings
    populationSize_              = 100   ;
    maxEvaluations_              = 100000 ;
    mutationProbability_         = .05;
    		//1.0/problem_.getNumberOfVariables() ;
    crossoverProbability_        = 0.9   ;
    mutationDistributionIndex_   = 20.0  ;
    crossoverDistributionIndex_  = 20.0  ;
  } // NSGAII_Settings


  /**
   * Configure NSGAII with default parameter experiments.settings
   * @return A NSGAII algorithm object
   * @throws jmetal.util.JMException
   */
  public Algorithm configure() throws JMException {
    Algorithm algorithm ;
    Selection  selection ;
    Crossover  crossover ;
    Mutation   mutation  ;

    HashMap  parameters ; // Operator parameters

    // Creating the algorithm. There are two choices: NSGAII and its steady-
    // state variant ssNSGAII
    algorithm = new NSGAII(problem_) ;
    //algorithm = new ssNSGAII(problem_) ;

    // Algorithm parameters
    algorithm.setInputParameter("populationSize",populationSize_);
    algorithm.setInputParameter("maxEvaluations",maxEvaluations_);
    algorithm.setInputParameter("ServicePool", AllServicePool);
    algorithm.setInputParameter("ClassPool", classes);
    algorithm.setInputParameter("BasicSolution",BasicChmomsome);

    // Mutation and Crossover for Real codification
    parameters = new HashMap() ;
    parameters.put("probability", crossoverProbability_) ;
    parameters.put("distributionIndex", crossoverDistributionIndex_) ;
    parameters.put("Solutionlength", 5) ;
    crossover = CrossoverFactory.getCrossoverOperator("QoSCrossover", parameters);

    parameters = new HashMap() ;
    parameters.put("probability", mutationProbability_) ;
    parameters.put("distributionIndex", mutationDistributionIndex_) ;
    
    mutation = MutationFactory.getMutationOperator("QoS_BitFlipMutation", parameters);

    // Selection Operator
    parameters = null ;
    selection = SelectionFactory.getSelectionOperator("BinaryTournament2", parameters) ;

    // Add the operators to the algorithm
    algorithm.addOperator("crossover",crossover);
    algorithm.addOperator("mutation",mutation);
    algorithm.addOperator("selection",selection);

    return algorithm ;
  } // configure

 /**
  * Configure NSGAII with user-defined parameter experiments.settings
  * @return A NSGAII algorithm object
  */
  @Override
  public Algorithm configure(Properties configuration) throws JMException {
	  System.out.println("configure1");
    Algorithm algorithm ;
    Selection  selection ;
    Crossover  crossover ;
    Mutation   mutation  ;

    HashMap  parameters ; // Operator parameters

    // Creating the algorithm. There are two choices: NSGAII and its steady-
    // state variant ssNSGAII
    algorithm = new NSGAII(problem_) ;
    //algorithm = new ssNSGAII(problem_) ;

    // Algorithm parameters
    populationSize_ = Integer.parseInt(configuration.getProperty("populationSize",String.valueOf(populationSize_)));
    maxEvaluations_  = Integer.parseInt(configuration.getProperty("maxEvaluations",String.valueOf(maxEvaluations_)));
    algorithm.setInputParameter("populationSize",populationSize_);
    algorithm.setInputParameter("maxEvaluations",maxEvaluations_);

    // Mutation and Crossover for Real codification
    crossoverProbability_ = Double.parseDouble(configuration.getProperty("crossoverProbability",String.valueOf(crossoverProbability_)));
    crossoverDistributionIndex_ = Double.parseDouble(configuration.getProperty("crossoverDistributionIndex",String.valueOf(crossoverDistributionIndex_)));
    parameters = new HashMap() ;
    parameters.put("probability", crossoverProbability_) ;
    parameters.put("distributionIndex", crossoverDistributionIndex_) ;
    parameters.put("Solutionlength", 6) ;
    crossover = CrossoverFactory.getCrossoverOperator("QoSCrossover", parameters);

    mutationProbability_ = Double.parseDouble(configuration.getProperty("mutationProbability",String.valueOf(mutationProbability_)));
    mutationDistributionIndex_ = Double.parseDouble(configuration.getProperty("mutationDistributionIndex",String.valueOf(mutationDistributionIndex_)));
    parameters = new HashMap() ;
    parameters.put("probability", mutationProbability_) ;
    parameters.put("distributionIndex", mutationDistributionIndex_) ;
    mutation = MutationFactory.getMutationOperator("QoS_BitFlipMutation", parameters);

    // Selection Operator
    parameters = null ;
    selection = SelectionFactory.getSelectionOperator("BinaryTournament2", parameters) ;

    // Add the operators to the algorithm
    algorithm.addOperator("crossover",crossover);
    algorithm.addOperator("mutation",mutation);
    algorithm.addOperator("selection",selection);

    return algorithm ;
  }
  
  
  
  
  private static void createBasic() {
System.out.println("createBasic1");
		Random random = new Random();
		BasicChmomsome=new ArrayList<Service>();
		int j=0;
		for (int i=0; i<= classes.size()-1; i++){
			fillarray(classes.get(i));
			j = random.nextInt(TempClass.size()-1);
			
			BasicChmomsome.add(TempClass.get(j));
			TempClass =new ArrayList<Service>();
			
		}
		

		
	}
	private static void fillarray(ServiceClass class1) {
		// TODO Auto-generated method stub
		
		
		for (int i=0;i<AllServicePool.size();i++){if (AllServicePool.get(i).getServiceClass().equals(class1.getClassid()))    
			{
			TempClass.add(AllServicePool.get(i));
			}
			}
		
	}

	
} // NSGAII_Settings
