package jmetal.experiments.settings;

import jmetal.core.Algorithm;
import jmetal.experiments.Settings;
import jmetal.metaheuristics.nsgaII.NSGAII;
import jmetal.operators.crossover.Crossover;
import jmetal.operators.crossover.CrossoverFactory;
import jmetal.operators.mutation.Mutation;
import jmetal.operators.mutation.MutationFactory;
import jmetal.operators.selection.Selection;
import jmetal.operators.selection.SelectionFactory;
import jmetal.problems.ProblemFactory;
import jmetal.util.JMException;

import java.util.HashMap;
import java.util.Properties;

import edu.birzeit.jmse.sbse.MoHrResources;

/**
 * Settings class of algorithm NSGA-II (real encoding)
 */
public class QoS_NSGAII_Settings extends Settings {
	public int stopTimeInSec_;
	public int populationSize_;
	public int maxEvaluations_;
	public double mutationProbability_;
	public double crossoverProbability_;
	public double mutationDistributionIndex_;
	public double crossoverDistributionIndex_;

	/**
	 * Constructor
	 * 
	 * @throws Exception
	 */
	// NSGAII_Settings

	public QoS_NSGAII_Settings(String problemName) {
		// TODO Auto-generated constructor stub
		super(problemName);
		String employeeFilePath =
		/*
		String bugsFileName = CommonSettings.PROJ_DIR + "/dataset/" + problemName + ".txt";
		String employeeFilePath;

		if (problemName.contains("JDT")) {
			employeeFilePath = CommonSettings.PROJ_DIR + "/dataset/JDTDeveloperWithWage.txt";
		}
		if (problemName.contains("exp_")) {
			employeeFilePath = CommonSettings.PROJ_DIR + "/dataset/exp_developers.txt";
		} else {
			employeeFilePath = CommonSettings.PROJ_DIR + "/dataset/PlatformDeveloperWithWage.txt";

		}
		
		*/
		
		problem_ = new ("MixedPermutation", bugsFileName, employeeFilePath);

		// Default experiments.settings
		stopTimeInSec_ = -1;
		populationSize_ = 100;
		maxEvaluations_ = 100000;
		mutationProbability_ = 1.0 / problem_.getNumberOfVariables()  ;
		crossoverProbability_ = 0.9;
		mutationDistributionIndex_ = 3;
		crossoverDistributionIndex_ = 3;
	}

	/**
	 * Configure NSGAII with default parameter experiments.settings
	 * 
	 * @return A NSGAII algorithm object
	 * @throws jmetal.util.JMException
	 */
	public Algorithm configure() throws JMException {
		Algorithm algorithm;
		Selection selection;
		Crossover crossover;
		Mutation mutation;

		HashMap parameters; // Operator parameters

		// Creating the algorithm. There are two choices: NSGAII and its steady-
		// state variant ssNSGAII
		algorithm = new NSGAII(problem_);
		// algorithm = new ssNSGAII(problem_) ;

		// Algorithm parameters
		algorithm.setInputParameter("populationSize", populationSize_);
		algorithm.setInputParameter("maxEvaluations", maxEvaluations_);
		algorithm.setInputParameter("stopTimeInSec", stopTimeInSec_);

		// Mutation and Crossover for Real codification
		parameters = new HashMap();
		parameters.put("probability", crossoverProbability_);
		parameters.put("distributionIndex", crossoverDistributionIndex_);
		crossover = CrossoverFactory.getCrossoverOperator("MixedPMXCrossover", parameters);

		parameters = new HashMap();
		parameters.put("probability", mutationProbability_);
		parameters.put("distributionIndex", mutationDistributionIndex_);
		mutation = MutationFactory.getMutationOperator("MixedPMXSwapMutation", parameters);

		// Selection Operator
		parameters = null;
		selection = SelectionFactory.getSelectionOperator("BinaryTournament", parameters);

		// Add the operators to the algorithm
		algorithm.addOperator("crossover", crossover);
		algorithm.addOperator("mutation", mutation);
		algorithm.addOperator("selection", selection);

		return algorithm;
	} // configure

	/**
	 * Configure NSGAII with user-defined parameter experiments.settings
	 * 
	 * @return A NSGAII algorithm object
	 */
	@Override
	public Algorithm configure(Properties configuration) throws JMException {
		Algorithm algorithm;
		Selection selection;
		Crossover crossover;
		Mutation mutation;

		HashMap parameters; // Operator parameters

		// Creating the algorithm. There are two choices: NSGAII and its steady-
		// state variant ssNSGAII
		algorithm = new NSGAII(problem_);
		// algorithm = new ssNSGAII(problem_) ;

		// Algorithm parameters
		populationSize_ = Integer
				.parseInt(configuration.getProperty("populationSize", String.valueOf(populationSize_)));
		maxEvaluations_ = Integer
				.parseInt(configuration.getProperty("maxEvaluations", String.valueOf(maxEvaluations_)));
		stopTimeInSec_ = Integer.parseInt(configuration.getProperty("stopTimeInSec", String.valueOf(stopTimeInSec_)));

		algorithm.setInputParameter("populationSize", populationSize_);
		algorithm.setInputParameter("maxEvaluations", maxEvaluations_);
		algorithm.setInputParameter("stopTimeInSec", stopTimeInSec_);

		// Mutation and Crossover for Real codification
		crossoverProbability_ = Double
				.parseDouble(configuration.getProperty("crossoverProbability", String.valueOf(crossoverProbability_)));
		crossoverDistributionIndex_ = Double.parseDouble(
				configuration.getProperty("crossoverDistributionIndex", String.valueOf(crossoverDistributionIndex_)));
		parameters = new HashMap();
		parameters.put("probability", crossoverProbability_);
		parameters.put("distributionIndex", crossoverDistributionIndex_);
		crossover = CrossoverFactory.getCrossoverOperator("MixedPMXCrossover", parameters);

		mutationProbability_ = Double
				.parseDouble(configuration.getProperty("mutationProbability", String.valueOf(mutationProbability_)));
		mutationDistributionIndex_ = Double.parseDouble(
				configuration.getProperty("mutationDistributionIndex", String.valueOf(mutationDistributionIndex_)));
		parameters = new HashMap();
		parameters.put("probability", mutationProbability_);
		parameters.put("distributionIndex", mutationDistributionIndex_);
		mutation = MutationFactory.getMutationOperator("MixedPMXSwapMutation", parameters);

		// Selection Operator
		parameters = null;
		selection = SelectionFactory.getSelectionOperator("BinaryTournament2", parameters);

		// Add the operators to the algorithm
		algorithm.addOperator("crossover", crossover);
		algorithm.addOperator("mutation", mutation);
		algorithm.addOperator("selection", selection);

		return algorithm;
	}
} // NSGAII_Settings
